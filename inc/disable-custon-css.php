<?php
// Disable Custom CSS
remove_action( 'wp_head', 'wp_custom_css_cb', 11 );
remove_action( 'wp_head', 'wp_custom_css_cb', 101 );

function disable_custom_css_customize_section( $wp_customize ) {
	$wp_customize->remove_section( 'custom_css' );
}
add_action( 'customize_register', 'disable_custom_css_customize_section', 20 );
