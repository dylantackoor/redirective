<?php

// Hide file editor
define( 'DISALLOW_FILE_EDIT', true );

// Remove "Howdy {username}"
function change_howdy($translated, $text, $domain) {
	if (!is_admin() || 'default' != $domain) {
		return $translated;
	}

	if (false !== strpos($translated, 'Howdy')) {
		return str_replace('Howdy, ', '', $translated);
	}

	return $translated;
}
add_filter('gettext', 'change_howdy', 10, 3);

// Remove "Thank you for creating with WordPress." + version
function hide_footer_text() {
	echo '<style type="text/css">#wpfooter {display:none;}</style>';
}
add_action('admin_head', 'hide_footer_text');
