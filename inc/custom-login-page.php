<?php
// Logo image
function login_logo() { ?>
	<style type="text/css">
		#login h1 a,
		.login h1 a {
			background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/logo.svg);
		}
	</style>
<?php }
add_action('login_enqueue_scripts', 'login_logo');

// Logo link
function login_logo_url() {
	// TODO: pull this from ACF option field
	return 'https://dylantackoor.com';
}
add_filter('login_headerurl', 'login_logo_url');

// Logo alt
function login_logo_url_title() {
	return 'Dylan Tackoor';
}
add_filter('login_headertitle', 'login_logo_url_title');
