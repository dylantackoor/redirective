<?php

// Hide Jetpack menu
function hide_jetpack() {
	if ( class_exists( 'Jetpack' ) ) {
		remove_menu_page( 'jetpack' );
	}
}
add_action( 'admin_init', 'hide_jetpack' );

function remove_jetpack_image_script() {
	wp_dequeue_script( 'devicepx' );
}
add_action( 'wp_enqueue_scripts', 'remove_jetpack_image_script' );

function remove_jetpack_modules( $modules ){
	$jp_mods_to_disable = array(
		//  'json-api',
		'related-posts',
		'lazy-images',
		'comment-likes',
		'search',
		'seo-tools',
		'google-analytics',
		'wordads',
		'sitemaps',
		'masterbar',
		'copy-post',
		'manage',
		'protect',
		'sso',
		'mobile-push',
		'photon',
		'stats',
		'gravatar-hovercards',
		'minileven',
		'vaultpress',
		'custom-css',
		'infinite-scroll',
		'wpcc',
		'shortcodes',
		'widget-visibility',
		'contact-form',
		'shortlinks',
		'tiled-gallery',
		'publicize',
		'post-by-email',
		'widgets',
		'comments',
		'latex',
		'enhanced-distribution',
		'notes',
		'subscriptions',
		'after-the-deadline',
		'carousel',
		'sharedaddy',
		'omnisearch',
		'likes',
		'videopress',
		'gplus-authorship',
		'monitor',
		'markdown',
		'verification-tools',
		'custom-content-types',
		'site-icon',
	);

	foreach ( $jp_mods_to_disable as $mod ) {
		if ( isset( $modules[$mod] ) ) {
			unset( $modules[$mod] );
		}
	}

	return $modules;
}
add_filter( 'jetpack_get_available_modules', 'remove_jetpack_modules' );
