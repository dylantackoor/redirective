<?
require_once get_stylesheet_directory() . '/vendor/tgmpa/tgm-plugin-activation/class-tgm-plugin-activation.php';

function require_plugins() {
	// Array of plugin arrays. Required keys are name and slug.
	$plugins = array(
		array(
			'name'               => 'LittleBot Netlify',
			'slug'               => 'littlebot-netlify',
			'required'           => true,
			// 'force_activation'   => true,
			'force_deactivation' => true,
			'source'             => get_stylesheet_directory() . '/plugins/littlebot-netlify.zip',
			'external_url'       => 'https://github.com/justinwhall/littlebot-netlify'
		),

		array(
			'name'               => 'Basic Auth',
			'slug'               => 'Basic-Auth',
			'required'           => true,
			'force_activation'   => true,
			'force_deactivation' => true,
			'source'             => get_stylesheet_directory() . '/plugins/Basic-Auth.zip',
			'external_url'       => 'https://github.com/WP-API/Basic-Auth'
		),

		array(
			'name'             => 'WP API Menus',
			'slug'             => 'wp-api-menus',
			'required'         => true,
			'force_activation' => false,
		),

		array(
			'name'             => 'Advanced Custom Fields',
			'slug'             => 'advanced-custom-fields',
			'required'         => false,
			'force_activation' => false,
		),

		array(
			'name'             => 'ACF to REST API',
			'slug'             => 'acf-to-rest-api',
			'required'         => false,
			'force_activation' => false,
		),

		array(
			'name'             => 'Yoast SEO',
			'slug'             => 'wordpress-seo',
			'required'         => false,
			'force_activation' => false,
		),

		array(
			'name'             => 'Hide SEO Bloat',
			'slug'             => 'so-clean-up-wp-seo',
			'required'         => false,
			'force_activation' => false,
		),

		array(
			'name'             => 'ACF Content Analysis for Yoast SEO',
			'slug'             => 'acf-content-analysis-for-yoast-seo',
			'required'         => false,
			'force_activation' => false,
		),
	);

	$config = array(
		'is_automatic' => true, // Automatically activate plugins after installation or not.
	);

	tgmpa( $plugins, $config );
}
add_action( 'tgmpa_register', 'require_plugins' );
