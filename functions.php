<?php

// Require Plugins
require_once 'inc/require-plugins.php';

// Customize login page
require_once 'inc/custom-login-page.php';

// Disable Custom CSS
require_once 'inc/disable-custon-css.php';

// Remove bloat
require_once 'inc/remove-bloat.php';
require_once 'inc/remove-jetpack-bloat.php';

// Require projects type
require_once 'inc/post-type-project.php';

// TODO: verify ACF pro requirement
// Add Headless Settings menu
// require_once 'inc/acf-options.php';
